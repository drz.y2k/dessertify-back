const dayjs = require('dayjs');
const { fileRegisterModel } = require('../models');
const { deleteObject } = require('../helpers/s3Methods.js');
const { getPresignedCurrentItems } = require('../helpers/s3Methods.js');

const listImages = async (req, res) => {

  let data = await fileRegisterModel.find({}).sort({ createdAt: 'descending' });


  let dataWithSignedUrl = await getPresignedCurrentItems(data.map(el => el.fileName));


  data = data.map((el, i) => { return { ...el._doc, signedURL: dataWithSignedUrl[i] } });

  res.json(data);
}

const createImageRegister = async (req, res) => {


  await fileRegisterModel.create({
    fileName: req.file.filename
  });

  await deleteImageRegister();

  res.json({ message: 'ok' })
}

const deleteImageRegister = async (req, res) => {
  let [{ _id: id, fileName },...rest] = await fileRegisterModel.find({}).sort({ createdAt: 'ascending' });

  if(rest.length>=12){
    await deleteObject(fileName);

    await fileRegisterModel.findByIdAndDelete(id);
  }




}


module.exports = { listImages, createImageRegister };
