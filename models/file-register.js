const mongoose = require('mongoose');

const FileRegisterScheme = new mongoose.Schema({
  fileName: {
    type: String,
  }
}, {
  timestamps: true, //TODO createdAt, updatedAt
  versionKey: false
});

module.exports = mongoose.model('fileregister', FileRegisterScheme);
