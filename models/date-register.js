const mongoose = require('mongoose');

const DateRegisterScheme = new mongoose.Schema({
  dateCode: {
    type: String,
  },
  registerCount: {
    type: Number,
  }
}, {
  timestamps: true, //TODO createdAt, updatedAt
  versionKey: false
});

module.exports = mongoose.model('dateregister', DateRegisterScheme);
