const customHeader = (req, res, next) => {
	try{
		const VALIDATOR_HEADER= req.headers['validator'];
		if(VALIDATOR_HEADER==process.env.VALIDATOR_HEADER){
			next()
		}else{
			res.status(403);
			res.send({error:'not authorized'})
		}
	}catch(e){
		res.status(403);
		res.send({error:'error'})
	}

}

module.exports = customHeader;
