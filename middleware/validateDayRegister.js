const dayjs = require('dayjs');
const { dateRegisterModel } = require('../models');

const validateDayRegister = async (req, res, next) => {
  try {
    const data = await dateRegisterModel.find({ dateCode: dayjs().format('YYYYMMDD') });


    if (data.length == 0) {
      const newDateRegister = new dateRegisterModel({ dateCode: dayjs().format('YYYYMMDD'), registerCount: 1 });
      await newDateRegister.save();
    } else {
      const { _id: id, registerCount } = data[0];
      if (registerCount >= 150) {
        res.status(400).json({ status: 'Limit Exceeded' });
        return;
      }

      await dateRegisterModel.updateOne({ _id: id }, { $set: { registerCount: registerCount + 1 } });
    }
  } catch (e) {
    console.log('err', e);

  }
  next();
};

module.exports = { validateDayRegister };
