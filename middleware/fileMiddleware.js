
const fs = require('fs');
const multer = require('multer');
const { v4: uuid } = require('uuid');
const { uploadObjectS3, deleteObject } = require('../helpers/s3Methods');
const { detectLabelsRekognition } = require('../helpers/rekognitionMethods');

const storage = multer.diskStorage({


  destination: function (req, file, cb) {

    let pathStorage = '';

    if (process.env.NODE_ENV === 'development') {
      pathStorage = `${__dirname}/../files`;
    } else {
      pathStorage = `/tmp`;
    }
    cb(null, pathStorage);

  },
  filename: function (req, file, cb) {
    const ext = file.originalname.split('.').pop();
    const filename = `${uuid()}.${ext}`
    cb(null, filename)
  }
});

const uploadMiddleware = multer({
  storage: storage,
});

const validateFileMiddleware = async (req, res, next) => {
  const { file } = req;
  await uploadObjectS3(file.filename);

  //delete file from local storage
  if (process.env.NODE_ENV === 'development') {
    fs.unlinkSync(`${__dirname}/../files/${file.filename}`);
  } else {
    fs.unlinkSync(`/tmp/${file.filename}`);
  }


  const errors = await detectLabelsRekognition(file.filename);

  if (errors.length > 0) {
    await deleteObject(file.filename);
    res.status(406).json({ errors });
    return;
  }



  next();
};




module.exports = { uploadMiddleware, validateFileMiddleware };
