
const mongoose = require('mongoose');


const dbConnect = async () => {
	const DB_URI = process.env.MONGODB_URI;
	try {

		await mongoose.connect(DB_URI, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		});
		console.log('**** CONEXION CORRECTA ****');
	} catch (err) {
		console.log('**** CONEXION FALLIDA ****');
	}
}

module.exports = dbConnect;
