const fs = require("fs");

const sharp = require("sharp");


const { S3Client, ListObjectsCommand, GetObjectCommand, PutObjectCommand, DeleteObjectCommand } = require("@aws-sdk/client-s3");
const { getSignedUrl } = require("@aws-sdk/s3-request-presigner");

const client = new S3Client({ region: process.env.region });

const getListObjectsS3 = async () => {

  let els = [];

  const listParams = {
    Bucket: process.env.BUCKET_NAME,
    MaxKeys: 50,
  };

  try {
    const data = await client.send(new ListObjectsCommand(listParams));
    els = data.Contents.map(item => item.Key);
  } catch (err) {
    console.log("Error", err);
  }

  return els;

}

const getObjectS3 = async (fileName) => {
  const getParams = {
    Bucket: process.env.BUCKET_NAME,
    Key: fileName,
  };

  const command = new GetObjectCommand(getParams);

  const url = await getSignedUrl(client, command, { expiresIn: 20 });
  return url;

}

const getPresignedCurrentItems = async (arrFiles) => {
  // let resArr = await getListObjectsS3();

  let arrProms = [];

  for (el of arrFiles) {
    arrProms.push(getObjectS3(el));
  }


  let resResp = await Promise.allSettled(arrProms);

  return resResp.map(el => el.value);

};

const uploadObjectS3 = async (fileName) => {

  let file;

  if (process.env.NODE_ENV === 'development') {
    file = fs.readFileSync(`files/${fileName}`);
  } else {
    file = fs.readFileSync(`/tmp/${fileName}`);
  }

  let compressed = await sharp(file).resize(500, null).jpeg().toBuffer();

  const uploadParams = {
    Body: compressed,
    Bucket: process.env.BUCKET_NAME,
    Key: fileName,
    ContentType: "image",

  }

  const commandUpload = new PutObjectCommand(uploadParams);
  const response = await client.send(commandUpload);
}

const deleteObject = async (fileName) => {

  const deleteParams = {
    Bucket: process.env.BUCKET_NAME,
    Key: fileName,
  }

  const command = new DeleteObjectCommand(deleteParams);
  await client.send(command);
  // console.log(res), 'deleted';

}


module.exports = { getListObjectsS3, getObjectS3, uploadObjectS3, deleteObject, getPresignedCurrentItems };
