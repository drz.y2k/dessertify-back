const { RekognitionClient, DetectLabelsCommand, DetectModerationLabelsCommand } = require("@aws-sdk/client-rekognition");

const client = new RekognitionClient({ region: process.env.region });

const detectLabelsRekognition = async (fileName) => {

  const errors = [];

  const input = {
    "Image": {
      "S3Object": {
        "Bucket": process.env.BUCKET_NAME,
        "Name": fileName
      }
    },
    "MaxLabels": 10,
    "MinConfidence": 90
  };
  const detectLabel = new DetectLabelsCommand(input);
  const detectModeration = new DetectModerationLabelsCommand(input);
  const responseLabel = await client.send(detectLabel);
  // console.log(response);
  const responseModeration = await client.send(detectModeration);
  if (responseModeration.ModerationLabels.length > 0) {
    errors.push("error_moderation");
  }

  if (!responseLabel.Labels.map((label) => label.Name).includes("Dessert")) {
    errors.push("error_dessert");

  }

  return errors;
}

module.exports = { detectLabelsRekognition };
