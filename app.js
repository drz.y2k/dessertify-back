

require("dotenv").config();

const express = require("express");
const cors = require("cors");

const dbConnect = require('./config/mongo');

const { uploadMiddleware, validateFileMiddleware } = require("./middleware/fileMiddleware");
const { validateDayRegister } = require("./middleware/validateDayRegister");
const customHeader = require("./middleware/customHeader");
const { listImages, createImageRegister } = require("./controllers/images");

const { rateLimit } = require('express-rate-limit');

const helmet = require('helmet');



const limiter = rateLimit({
  windowMs: 1 * 60 * 1000,
  max: 15
})

const app = express();
app.use(helmet());
app.use(limiter);
app.use(cors());


const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log('running on port:' + port);
})

app.post('/upload-image', validateDayRegister, uploadMiddleware.single("myfile"), validateFileMiddleware, createImageRegister);

app.get('/list-images', listImages);

app.get('/test', (req, res) => {
  res.json({ message: process.env.NODE_ENV });
});

dbConnect();

module.exports = app;
